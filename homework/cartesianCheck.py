'''
les inn float
sjekk hvilke kvadrant punktet er i
skriv ut hvilke kvadrant punktet er i

Q4|Q1
-----
Q3|Q2

'''
############## CODE TO HAND IN START ################
#X,Y = map(float, input() .split())


#if (X and Y) == 0:
#    print('Origem')
#elif (X > 0) and (Y < 0):
#    print ('Q4')
#elif (X < 0) and (Y < 0):
#    print('Q3')
#elif (X < 0) and (Y > 0):
#    print('Q2')
#else:
#    print('Q1')

############## CODE TO HAND IN END #################


## section with better feedback and test cases ##
# Scenario Origo : X is 0 Y is 0
X=0
Y=0
# Scenario Q1 - both positive
X=1.0
Y=1.0
# Scenario Q2 - X positive Y negative
X=2.0
Y=-2.0
# Scenario Q3 - X negative Y negative
X=-3.0
Y=-3.0
# Scenario Q4 - X negative Y positive
X=-4.0
Y=4.0

if (X and Y) == 0:
    print("Origem")
    print("X is:", X)
    print("Y is:", Y)
elif (X > 0) and (Y < 0):
    print ("Q4")
    print("X is:", X)
    print("Y is:", Y)
elif (X < 0) and (Y < 0):
    print("Q3")
    print("X is:", X)
    print("Y is:", Y)
elif (X < 0) and (Y > 0):
    print("Q2")
    print("X is:", X)
    print("Y is:", Y)
else:
    print("Q1")
    print("X is:", X)
    print("Y is:", Y)

